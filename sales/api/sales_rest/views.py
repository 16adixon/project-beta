import json
import requests
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Salesperson, Customer, Sale
from django.http import JsonResponse
from common.json import ModelEncoder

class AutomobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["sold", "import_vin"]

class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id"]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "phone_number", "address"]

class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "customer", "salesperson", "price"]
    encoders = {"automobile": AutomobileVOListEncoder(), "salesperson": SalespersonListEncoder(), "customer": CustomerListEncoder()}



@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonListEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def api_show_salesperson(request, pk):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(employee_id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalespersonListEncoder,
            safe=False
        )
    else:
        count, _ = Salesperson.objects.get(employee_id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def api_show_customer(request, pk):
    if request == "GET":
        customer = Customer.objects.get(phone_number=pk)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False
        )
    else:
        count, _ = Customer.objects.get(phone_number=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        sales = {}
        try:
            vin = content["import_vin"]
            automobile = AutomobileVO.objects.get(import_vin=vin)
            sales["automobile"] = automobile

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid vin"},
                status=400
            )

        try:
            salesperson_href = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=salesperson_href)
            sales["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "invalid salesperson id"},
                status=400
        )

        try:
            customer_href = content["customer"]
            customer = Customer.objects.get(phone_number=customer_href)
            sales["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "invalid customer id"},
                status=400
            )

        price = content["price"]
        sales["price"] = price

        sale = Sale.objects.create(**sales)
        sold = {
            "sold": True
        }
        requests.put(f'http://project-beta-inventory-api-1:8000/api/automobiles/{vin}/', json=sold)

        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False
        )


def api_delete_sale(request, vin):
    sale = Sale.objects.filter(automobile__import_vin=vin)
    sale.delete()
    return JsonResponse(
        {"Deleted": "True"}
    )


# def update_sold(request, vin):
#     sales = Sale.objects.all()
#     auto = AutomobileVO.objects.get(import_vin=vin)
#     if auto.sold in sales:
#         auto.sold = True
#         auto.save()
#     else:
#         auto.sold = False
#         auto.save()
