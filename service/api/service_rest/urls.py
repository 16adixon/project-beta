from django.urls import path
from .views import api_list_technicians, api_list_appointments, api_technicians_details, api_appointment_detail

urlpatterns = [
    path('technicians/', api_list_technicians, name="api_list_technicians"),
    path('appointments/', api_list_appointments, name="api_list_appointments"),
    path('technicians/<int:pk>/', api_technicians_details, name="api_technicians_details"),
    path('appointments/<str:vin>/', api_appointment_detail, name="api_appointment_detail"),
]