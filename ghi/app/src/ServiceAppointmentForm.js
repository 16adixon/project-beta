import { useEffect, useState } from "react"

function AppointmentForm () {
    const [technician, setTechnician] = useState([])
    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState('')
    const [technicians, setTechnicians] = useState('')
    const [reason, setReason] = useState('')
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')

    const handleVinChange = (event) => {
      const value = event.target.value
      setVin(value)
    }

    const handleCustomerChange = (event) => {
      const value = event.target.value
      setCustomer(value)
    }

    const handleTechniciansChange = (event) => {
      const value = event.target.value
      setTechnicians(value)
    }

    const handleReasonChange = (event) => {
      const value = event.target.value
      setReason(value)
    }

    const handleDateChange = (event) => {
      const value = event.target.value;
      setDate(value);
      console.log(date)
    }

    const handleTimeChange = (event) => {
      const value = event.target.value;
      setTime(value);
    }

    

    const getData = async () => {
        const url = "http://localhost:8080/api/technicians/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technicians)
        }
    }
    useEffect(() => {
        getData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        var dates = document.getElementById('date').value;
        var times = document.getElementById('time').value;
        var dateTimeValue = `${dates} ${times}`
        var date_time = new Date(dateTimeValue);

        const data = {}
        data.vin = vin;
        data.customer = customer;
        data.technician = technicians;
        data.reason = reason;
        data.date_time = date_time;

        const url = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
          setVin('');
          setCustomer('');
          setTechnicians('');
          setReason('');
          setDate('');
          setTime('');
            
          console.log("sent")
        } else {
          console.log(response)
        }
        
    }
  
    return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a service appointment</h1>
          <form onSubmit={handleSubmit} id="create-service-appointment-form">
            <div className="form-floating mb-3">
              <input onChange={handleVinChange} value={vin} placeholder="Automobile_vin" required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="vin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
              <label htmlFor="customer">Customer</label>
            </div>

            
            <div className="form-floating mb-3">
              <input onChange={handleDateChange} value={date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
              <label htmlFor="date">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleTimeChange} value={time} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
              <label htmlFor="time">Time</label>
            </div>
            

            <div className="mb-3">
              <select onChange={handleTechniciansChange} value={technicians} required name="technician" id="technician" className="form-select">
                <option value="">Choose a technician</option>
                {technician.map(technicians => {
                  return (
                    <option key={technicians.employee_id} value={technicians.employee_id}>{technicians.first_name}</option>
                  )
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
      </div>
    </div>
    )
}
export default AppointmentForm;