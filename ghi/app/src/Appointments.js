import { useEffect, useState } from "react";


function Appointments () {
    const [appointments, setAppointments] = useState([])
    const [sold, setSold] = useState([])

    const getSales = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
            const data = await response.json();
            setSold(data.autos)
        }
    }
    useEffect(() => {
        getSales()
    }, [])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/')
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
            console.log(data.appointments)
        }
    }
    useEffect (() => {
        getData()
    }, [])

    async function Finished(vin) {
        const data = {}
        data.status = "finished"
        const url = `http://localhost:8080/api/appointments/${vin}/`
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            console.log("updated")
        } else {
            console.log("Didnt update")
        }
    }

    async function Canceled(vin) {
        const data = {"status": "Canceled"}
        const url = `http://localhost:8080/api/appointments/${vin}/`
        // const newUrl = url.replace(/[%20+]/gi, "")
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            console.log("updated")
        } else {
            console.log("Didnt update")
        }
    }

    function isVip(vin) {
        let vins = sold.map(sale => sale['vin'])
        console.log(vins)
        if (vins.includes(vin)) {
            return "yes"
        } else {
            return "no"
        }
    }

    return (
        <>
        <h1>Appointments</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            {appointments.map(appointment => {
                    return (
                        <tr key={appointment.vin}>
                            <td>{ appointment.vin }</td>
                            <td>{ isVip(appointment.vin) }</td>
                            <td>{ appointment.customer }</td>
                            <td id="date">{appointment.date_time.substring(0,10)}</td>
                            <td id="time">{appointment.date_time.substring(11, 19)}</td>
                            <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                            <td>{ appointment.reason }</td>
                            <td>
                                <button onClick={() => { Canceled(appointment.vin)}}>Cancel</button>
                                <button onClick={() => { Finished(appointment.vin)}}>Finish</button>
                            </td>
                        </tr>
                )
            })}
             </tbody>
        </table>
    </>
    )
}
export default Appointments;





    // const [datetime, setDateTime] = useState('')

    // setDateTime(appointments.date_time)

    // const getDateTime = async () => {
    //     const response = await fetch(`http://localhost:8080/api/appointments/${vin}`)
    //     console.log(response)
    //     if (response.ok) {
    //         const data = await response.json();
    //         setDateTime(data.date_time)
    //     }
    // }

    // useEffect (() => {
    //     getDateTime()
    // }, [])



    // const datetime = appointments.date_time
    // const dateTimeObject = new Date(datetime)
    // const date = dateTimeObject.toISOString().split('T')[0];
    // const time = dateTimeObject.toISOString().split('T')[1];
    // document.getElementById('date').value = date
    // document.getElementById('time').value = 243


    // const datetime = (appointments["date_time"]) => {

    //     date, time = datetime.split()
    //     let date = date
    //     let time = time
    // }

    // let dateTimeObject = new Date({datetime})
    // let date = dateTimeObject.toDateString()
    // let time = dateTimeObject.toTimeString()

    // document.getElementById("time").value = time

    // document.getElementById("cancel").addEventListener("click", cancel);
    // function cancel() {

    // }
