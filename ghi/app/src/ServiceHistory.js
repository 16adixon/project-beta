import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

function AppointmentHistory () {
    const [appointments, setAppointments] = useState([])
    const [vinSearch, setVinSearch] = useState('')
    const [sold, setSold] = useState([])

    const getSales = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
            const data = await response.json();
            setSold(data.autos)
        }
    }
    useEffect(() => {
        getSales()
    }, [])
    
    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/')
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }
    useEffect (() => {
        getData()
    }, [])


    const handleSearchChange = (event) => {
        const value = event.target.value
        setVinSearch(value)
      }


    function FilteredAppointments() {
        return appointments.filter(a => a.vin.toLowerCase().includes(vinSearch.toLowerCase()))
    }


    function isVip(vin) {
        let vins = sold.map(sale => sale['vin'])
        console.log(vins)
        if (vins.includes(vin)) {
            return "yes"
        } else {
            return "no"
        }
    }

    return (
        <>
        <h1>Service History</h1>
        <div className="input-group mb-3">
            <input onChange={handleSearchChange}  type="text" className="form-control" placeholder="Search by VIN..." aria-label="Recipient's username" aria-describedby="basic-addon2" />
            <div className="input-group-append">
                <span className="input-group-text" id="basic-addon2">Search</span>
            </div>
        </div>

        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
            {FilteredAppointments().map(appointment => {
                return (
                    <tr key={appointment.vin}>
                        <td>{ appointment.vin }</td>
                        <td>{ isVip(appointment.vin) }</td>
                        <td>{ appointment.customer }</td>
                        <td>{ appointment.date_time.substring(0,10) }</td>
                        <td>{ appointment.date_time.substring(11, 19) }</td>
                        <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                        <td>{ appointment.reason }</td>
                        <td>{ appointment.status }</td>
                    </tr>
                )
            })}
             </tbody>
        </table>
        </>
    )
}
export default AppointmentHistory;