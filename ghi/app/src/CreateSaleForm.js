import React, { useState, useEffect } from 'react'

function CreateSale() {
    const [salespeople, setSalesperson] = useState([])
    const [customers, setCustomer] = useState([])
    const [automobiles, setAutomobiles] = useState([])
    const [formData, setFormData] = useState({
        price: '',
        salesperson: '',
        customer: '',
        import_vin: ''
    })

    const getData = async () => {
        const salespersonFkUrl = 'http://localhost:8090/api/salespeople/'
        const customerFkUrl = 'http://localhost:8090/api/customers/'
        const automobileUrl = 'http://localhost:8100/api/automobiles/'
        const salespersonFkResponse = await fetch(salespersonFkUrl)
        const customerFkResponse = await fetch(customerFkUrl)
        const automobileResponse = await fetch(automobileUrl)

        if (salespersonFkResponse.ok) {
            const salespersonData = await salespersonFkResponse.json()
            setSalesperson(salespersonData.salespeople)
        } else {
            console.error("Bad response: salespersonFkUrl")
        }
        if (customerFkResponse.ok) {
            const customerData = await customerFkResponse.json()
            setCustomer(customerData.customers)
        } else {
            console.error("Bad response: customerFkUrl")
        }
        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json()
            setAutomobiles(automobileData.autos)
        } else {
            console.error("Bad response: automobileUrl")
        }
    }

    useEffect(() => {
        getData()
    }, [])



    const handleSubmit = async (event) => {
        event.preventDefault()

        const saleUrl = 'http://localhost:8090/api/sales/'
        formData.sold = true

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(saleUrl, fetchConfig)

        if (response.ok) {
            setFormData({
                price: '',
                salesperson: '',
                customer: '',
                import_vin: ''
            })
        } else {
            console.error("Bad response: saleUrl")
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name
        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    function FilterSold() {
        return automobiles.filter(automobile => automobile.sold === false)
    }

    return (
    <div className="row">
    <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Record a new sale</h1>
                <form onSubmit={handleSubmit} id="create-sale-form">
                    <div className="mb-3">
                        <select value={formData.import_vin} onChange={handleFormChange} required name="import_vin" id="import_vin" className="form-select">
                            <option value="">Choose an automobile VIN</option>
                            {FilterSold().map(automobile => {
                                return (
                                    <option key={automobile.id} value={automobile.vin}>
                                        {automobile.vin}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select value={formData.salesperson} onChange={handleFormChange} required name="salesperson" id="salesperson" className="form-select">
                            <option value="">Choose a salesperson</option>
                            {salespeople.map(salesperson => {
                                return (
                                    <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                        {salesperson.first_name} {salesperson.last_name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select value={formData.customer} onChange={handleFormChange} required name="customer" id="customer" className="form-select">
                            <option value="">Choose a customer</option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.phone_number} value={customer.phone_number}>
                                        {customer.first_name} {customer.last_name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={formData.price} onChange={handleFormChange} placeholder="Price" name="price" id="price" className="form-control"></input>
                        <label htmlFor="price">Price</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>

    )

}

export default CreateSale
