import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespeopleList from './Salespeople';
import SalespersonDetails from './SalespersonDetails';
import CustomerList from './Customers';
import SalesList from './Sales';
import CreateSaleForm from './CreateSaleForm';
import CreateSalesperson from './CreateSalespersonForm';
import CreateCustomerForm from './CreateCustomerForm';
import CreateManufacturer from './CreateManufacturerForm';
import ModelForm from './ModelForm';
import SalesHistory from './SalesHistory';
import TechniciansList from './Technicians';
import ManufacturersList from './Manufacturers';
import ModelsList from './Models';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './ServiceAppointmentForm';
import AppointmentHistory from './ServiceHistory';
import Appointments from './Appointments';
import Automobile from './Automobile';
import AutomobileForm from './AutomobileForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="" element={<ManufacturersList />} />
            <Route path="create" element={<CreateManufacturer />} />
          </Route>
          <Route path="models">
            <Route path="" element={<ModelsList />} />
            <Route path="create" element={<ModelForm />} />
          </Route>
          <Route path="salespeople">
            <Route path="" element={<SalespeopleList />} />
            <Route path=":employee_id" element={<SalespersonDetails />} />
            <Route path="create" element={<CreateSalesperson />} />
          </Route>
          <Route path="customers">
            <Route path="" element={<CustomerList />} />
            <Route path="create" element={<CreateCustomerForm />} />
          </Route>
          <Route path="sales">
            <Route path="" element={<SalesList />} />
            <Route path="create" element={<CreateSaleForm />} />
            <Route path="history" element={<SalesHistory />} />
          </Route>
          <Route path="technicians">
            <Route path="" element={<TechniciansList />} />
            <Route path="create" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route path="" element={<Appointments />} />
            <Route path="create" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<Automobile />} />
            <Route path="create" element={<AutomobileForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
