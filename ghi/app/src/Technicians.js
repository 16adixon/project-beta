import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function TechniciansList() {
    const[technicians, setTechnicians] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/')
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }
    
    useEffect(() => {
        getData()
    }, [])

    return (
        <>
        <h1>Technicians</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
            {technicians.map(technician => {
                    return (
                        <tr key={technician.employee_id}>
                            <td><Link to={technician.employee_id}>{technician.employee_id }</Link></td>
                            <td>{ technician.first_name }</td>
                            <td>{ technician.last_name }</td>
                        </tr>
                )
            })}
             </tbody>
        </table>
    </>
    )
}
export default TechniciansList;