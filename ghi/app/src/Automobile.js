import { useEffect, useState } from 'react';

function Automobile() {
    const [automobiles, setAutomobiles] = useState([])
    const [sales, setSales] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')

        if (response.ok) {
            const data = await response.json()
            setAutomobiles(data.autos)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    const getSoldData = async () => {
        const dataResponse = await fetch('http://localhost:8090/api/sales/')

        if (dataResponse.ok) {
            const salesData = await dataResponse.json()
            setSales(salesData.sales)
    }
    }
    useEffect(() =>{
        getSoldData()
    }, [])

    function Sold(vin) {
        console.log(vin)
        console.log(sales)
        if (sales.includes(vin)) {
            return "Sold"
        } else {
            return "Unsold"
        }

    }





    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
                {automobiles.map(automobile => {
                    return (
                        <tr key={automobile.vin}>
                            <td>{automobile.vin}</td>
                            <td>{automobile.color}</td>
                            <td>{automobile.year}</td>
                            <td>{automobile.model.name}</td>
                            <td>{automobile.model.manufacturer.name}</td>
                            <td>{String(automobile.sold)}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default Automobile
