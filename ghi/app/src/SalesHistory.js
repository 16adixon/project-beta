import React, { useEffect, useState } from 'react'


function SalesHistory() {
    const [salespeople, setSalespeople] = useState([])
    const [selectedSalesperson, setSelectedSalesperson] = useState("")
    const [sales, setSales] = useState([])

    const getSales = async () => {
        const response = await fetch('http://localhost:8090/api/sales/')

        if (response.ok) {
            const data = await response.json()
            setSales(data.sales)
        }
    }

    useEffect(() => {
        getSales()
    }, [])

    const getSalespeople = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/')

        if (response.ok) {
            const data = await response.json()
            setSalespeople(data.salespeople)
        } else {
            console.error("Bad response from api/salespeople")
        }
    }

    useEffect(() => {
        getSalespeople()
    }, [])



    const handleSalespersonChange = (e) => {
        const selectedemployee_id = e.target.value
        console.log(selectedemployee_id)
        setSelectedSalesperson(selectedemployee_id)
        // getSalesBySalesperson(selectedemployee_id)
        // getSales()
    }

    // const getSalesBySalesperson = async () => {
    //     const employeeId = handleSalespersonChange().selectedemployee_id
    //     console.log(employeeId)
    //     const response = await fetch(`http://localhost:8090/api/salespeople/${employeeId}/`)

    //     if (response.ok) {
    //         const data = await response.json()
    //         setSelectedSalesperson(data)
    //     } else {
    //         console.error('Bad response when trying to get specific salesperson')
    //     }
    // }

    // useEffect(() => {
    //     getSalesBySalesperson()
    // }, [])

    function FilteredSales() {
        // let allSales = sales.map(sale => sale)
        // console.log(allSales)
        // return allSales.filter(selectedSalesperson)

        // return sales.filter(sale => sale.includes(734))
        console.log(sales)
        console.log(selectedSalesperson)
        console.log(sales.filter(s => s.salesperson.employee_id === selectedSalesperson))
        return sales.filter(s => s.salesperson.employee_id === parseInt(selectedSalesperson))

    }


    return (
        <>
            {/* <h1>Salesperson History</h1>
            <select onChange={handleSalespersonChange} value={selectedSalesperson}>
                <option value="">Salespeople</option>
                {salespeople.map((salesperson) => (
                    <option key={salesperson.employee_id} value={salesperson.employee_id}>
                        {salesperson.first_name} {salesperson.last_name}
                    </option>
                ))}
            </select> */}

            <h1>Salesperson History</h1>
            <select onChange={handleSalespersonChange} value={selectedSalesperson}>
                <option value="">Salespeople</option>
                {salespeople.map(salesperson => {
                    return (
                    <option key={salesperson.employee_id} value={salesperson.employee_id}>
                        {salesperson.first_name} {salesperson.last_name}
                    </option>
                    )
                })}
            </select>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                {FilteredSales().map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.import_vin}</td>
                                <td>{sale.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}
export default SalesHistory;
