import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <div className="container-fluid">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li>
              <NavLink className="nav-link active" to="appointments/create">Create a Service Appointment</NavLink>
              <NavLink className="nav-link active" to="appointments">Service Appointments</NavLink>

            </li>
            <li>
              <NavLink className="nav-link active" to="sales/create">Add a Sale</NavLink>
              <NavLink className="nav-link active" to="sales">Sales</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active" to="manufacturers/create">Create a Manufacturer</NavLink>
              <NavLink className="nav-link active" to="manufacturers">Manufacturers</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active" to="models/create">Create a Model</NavLink>
              <NavLink className="nav-link active" to="models">Models</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active" to="automobiles/create">Create an Automobile</NavLink>
              <NavLink className="nav-link active" to="automobiles">Automobiles</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active" to="customers/create">Add a Customer</NavLink>
              <NavLink className="nav-link active" to="customers">Customers</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active" to="salespeople/create">Add a Salesperson</NavLink>
              <NavLink className="nav-link active" aria-current="page" to="/salespeople">Salespeople</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active" to="technicians/create">Add a Technician</NavLink>
              <NavLink className="nav-link active" to="technicians">Technicians</NavLink>
            </li>
            <li>
              <NavLink className="nav-link active" to="sales/history">Sales History</NavLink>
              <NavLink className="nav-link active" to="appointments/history">Service History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
