from django.shortcuts import render
from .models import Technician, AutomobileVO, Appointment
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from common.json import ModelEncoder

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id"]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["vin", "customer", "date_time", "technician", "reason", "status"]
    encoders = {"technician": TechnicianListEncoder()}

class AppointmentDetailEncoder(ModelEncoder):
    model= Appointment
    properties = ["date_time", "reason", "status", "vin", "customer", "technician"]
    encoders = {"technician": TechnicianListEncoder()}
    # def get_extra_data(self, o):
    #     return {"status": str(o.status)}

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder= TechnicianListEncoder,
            safe= False
        )
    else:
        content = json.loads(request.body)
        technicians = Technician.objects.create(**content)
        return JsonResponse(
            technicians,
            encoder= TechnicianListEncoder,
            safe=False
        )

@require_http_methods(["GET", "DELETE"])
def api_technicians_details(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(employee_id=pk)
            print("hi")
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "invalid technician"}, status = 400)
    else:
        try:
            count, _ = Technician.objects.filter(employee_id=pk).delete()
            return JsonResponse({"deleted": count>0})
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid Technician"}, status = 400)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician = Technician.objects.get(employee_id=content["technician"])
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid Technician"}, status=400)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            {"appointment": appointment},
            encoder= AppointmentListEncoder,
            safe=False
        )
    
@require_http_methods(["PUT", "GET", "DELETE"])
def api_appointment_detail(request, vin):
    if request.method == "GET":
        appointment = Appointment.objects.get(vin=vin)
        return JsonResponse(
            appointment,
            encoder = AppointmentDetailEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Appointment.objects.filter(vin=vin).update(**content)
        appointment = Appointment.objects.get(vin=vin)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False
        )
    else:
        count, _ = Appointment.objects.filter(vin=vin).delete()
        return JsonResponse({"deleted": count>0})